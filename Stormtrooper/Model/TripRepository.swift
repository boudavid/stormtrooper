//
//  TripRepository.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

protocol TripRepositoryType: class {
    func getTrips(
        successHandler: @escaping (([Trip]) -> Void),
        failureHandler: @escaping ((VideoRepositoryError) -> Void)
    )
}

enum VideoRepositoryError: String, Error {
    case getTripsFailed = "Sorry, we cannot retrieve the list of travels !"
}

final class TripRepository: TripRepositoryType {
    // MARK: - Properties

    private let tripRequest: TripRequestType

    // MARK: - Init

    init(tripRequest: TripRequestType) {
        self.tripRequest = tripRequest
    }

    func getTrips(
        successHandler: @escaping (([Trip]) -> Void),
        failureHandler: @escaping ((VideoRepositoryError) -> Void)
    ) {
        tripRequest.getAll { result in
            switch result {
            case .success(let trips):
                successHandler(trips)
            case .failure:
                failureHandler(.getTripsFailed)
            }
        }
    }
}
