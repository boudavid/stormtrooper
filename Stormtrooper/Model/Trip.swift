//
//  Trip.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

struct Trip: Decodable {
    let id: Int
    let pilot: Pilot
    let distance: Distance
    let duration: Int
    let pickUp: PointInformation
    let dropOff: PointInformation
}

struct Pilot: Decodable {
    let name: String
    let avatar: String
    let rating: Double
}

struct Distance: Decodable {
    let value: Int
    let unit: String
}

struct PointInformation: Decodable {
    let name: String
    let picture: String
    let date: Date
}
