//
//  ImageBuilder.swift
//  Stormtrooper
//
//  Created by David Bou on 01/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

protocol ImageBuilderType: class {
    func build(from imagePath: String) -> UIImage?
}

final class ImageBuilder: ImageBuilderType {
    // MARK: - Properties

    private let baseURL = "https://starwars.chauffeur-prive.com"

    // MARK: - ImageBuilderType

    func build(from imagePath: String) -> UIImage? {
        guard let url = URL(string: "\(baseURL)/\(imagePath)"),
            let imageData = try? Data(contentsOf: url)
        else {
            return nil
        }
        return UIImage(data: imageData)
    }
}
