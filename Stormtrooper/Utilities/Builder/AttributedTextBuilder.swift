//
//  AttributedTextBuilder.swift
//  Stormtrooper
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation
import UIKit

protocol AttributedTextBuilderType: class {
    func build(
        string: String,
        color: UIColor,
        size: CGFloat,
        weight: UIFont.Weight,
        spacing: CGFloat
    ) -> NSMutableAttributedString
}

final class AttributedTextBuilder: AttributedTextBuilderType {
    func build(
        string: String,
        color: UIColor,
        size: CGFloat,
        weight: UIFont.Weight,
        spacing: CGFloat
    ) -> NSMutableAttributedString {
        return NSMutableAttributedString(
            string: string,
            attributes: [
                .font: UIFont.systemFont(ofSize: size, weight: weight),
                .foregroundColor: color,
                .kern: spacing
            ]
        )
    }
}
