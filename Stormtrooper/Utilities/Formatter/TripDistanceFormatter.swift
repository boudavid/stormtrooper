//
//  TripDistanceFormatter.swift
//  Stormtrooper
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

protocol TripDistanceFormatterType: class {
    func format(
        distance: Int,
        unit: String
    ) -> String
}

final class TripDistanceFormatter: TripDistanceFormatterType {
    func format(
        distance: Int,
        unit: String
    ) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal

        let distanceFormatted = numberFormatter.string(from: NSNumber(value: distance)) ?? ""

        return "\(distanceFormatted) \(unit.uppercased())"
    }
}
