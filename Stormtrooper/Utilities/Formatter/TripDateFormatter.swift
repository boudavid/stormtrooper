//
//  TripDateFormatter.swift
//  Stormtrooper
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

protocol TripDateFormatterType: class {
    func format(date: Date) -> String
}

final class TripDateFormatter: TripDateFormatterType {
    func format(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "h:mm a"
        return formatter.string(from: date)
    }
}
