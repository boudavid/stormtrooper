//
//  UIView+Utilities.swift
//  Stormtrooper
//
//  Created by David Bou on 01/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

extension UIView {
    public class var storyboardIdentifier: String {
        return String(describing: self)
    }

    public class var nib: UINib {
        return UINib.init(nibName: storyboardIdentifier, bundle: Bundle(for: self))
    }

    func toCircle(of color: UIColor, borderWidth: CGFloat) {
        layer.cornerRadius = frame.width / 2
        layer.borderWidth = borderWidth
        layer.borderColor = color.cgColor
        layer.masksToBounds = true
    }
}
