//
//  TripDuration.swift
//  Stormtrooper
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

protocol TripDurationType: class {
    func getDuration(
        between firstDate: Date,
        and secondDate: Date
    ) -> DateComponents
}

final class TripDuration: TripDurationType {
    func getDuration(
        between firstDate: Date,
        and secondDate: Date
    ) -> DateComponents {
        return Calendar.current.dateComponents(
            [
                .hour,
                .minute,
                .second
            ],
            from: firstDate,
            to: secondDate
        )
    }
}
