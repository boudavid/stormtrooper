//
//  RatingView.swift
//  Stormtrooper
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

final class RatingView: UIView {
    // MARK: - Properties

    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var firstStarImage: UIImageView!
    @IBOutlet private weak var secondStarImage: UIImageView!
    @IBOutlet private weak var thirdStarImage: UIImageView!
    @IBOutlet private weak var fourthStarImage: UIImageView!
    @IBOutlet private weak var fifthStarImage: UIImageView!

    var rating: Double = 0 {
        didSet {
            setNeedsLayout()
        }
    }

    // MARK: - Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    // MARK: - Public

    override func layoutSubviews() {
        super.layoutSubviews()
        firstStarImage.image = rating >= 1 ? #imageLiteral(resourceName: "filled_star") : #imageLiteral(resourceName: "empty_star")
        secondStarImage.image = rating >= 2 ? #imageLiteral(resourceName: "filled_star") : #imageLiteral(resourceName: "empty_star")
        thirdStarImage.image = rating >= 3 ? #imageLiteral(resourceName: "filled_star") : #imageLiteral(resourceName: "empty_star")
        fourthStarImage.image = rating >= 4 ? #imageLiteral(resourceName: "filled_star") : #imageLiteral(resourceName: "empty_star")
        fifthStarImage.image = rating >= 5 ? #imageLiteral(resourceName: "filled_star") : #imageLiteral(resourceName: "empty_star")
    }

    // MARK: - Private

    private func initialize() {
        Bundle(for: type(of: self)).loadNibNamed(
            "RatingView",
            owner: self,
            options: nil
        )
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [
            .flexibleWidth,
            .flexibleHeight
        ]
    }
}
