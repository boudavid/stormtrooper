//
//  AppCoordinator.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

final class AppCoordinator {
    // MARK: - Properties

    private let presenter: UINavigationController
    private let screens: Screens

    // MARK: - Init

    init(
        presenter: UINavigationController,
        screens: Screens
    ) {
        self.presenter = presenter
        self.screens = screens
    }

    // MARK: - Navigation

    func start() {
        navigateToTripList()
    }

    private func navigateToTripList() {
        let controller = screens.tripListViewController(delegate: self)

        presenter.pushViewController(
            controller,
            animated: true
        )
    }

    private func navigateToTripDetail(trip: Trip) {
        let controller = screens.tripDetailViewController(
            delegate: self,
            trip: trip
        )

        presenter.pushViewController(
            controller,
            animated: true
        )
    }
}

// MARK: - TripListScreenDelegate

extension AppCoordinator: TripListScreenDelegate {
    func didSelectTrip(_ trip: Trip) {
        navigateToTripDetail(trip: trip)
    }
}

// MARK: - TripDetailScreenDelegate

extension AppCoordinator: TripDetailScreenDelegate {
    func didGoBack() {
        presenter.popViewController(animated: true)
    }
}
