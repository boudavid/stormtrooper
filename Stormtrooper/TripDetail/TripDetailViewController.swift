//
//  TripDetailViewController.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

final class TripDetailViewController: UIViewController {
    // MARK: - Properties

    @IBOutlet private weak var pickUpImage: UIImageView!
    @IBOutlet private weak var dropOffImage: UIImageView!
    @IBOutlet private weak var pilotAvatarImage: UIImageView! {
        didSet {
            pilotAvatarImage.toCircle(of: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25), borderWidth: 1)
        }
    }
    @IBOutlet private weak var pilotNameLabel: UILabel!
    @IBOutlet private weak var departureLabel: UILabel!
    @IBOutlet private weak var arrivalLabel: UILabel!
    @IBOutlet private weak var pickUpNameLabel: UILabel!
    @IBOutlet private weak var dropOffLabel: UILabel!
    @IBOutlet private weak var departureTimeLabel: UILabel!
    @IBOutlet private weak var arrivalTimeLabel: UILabel!
    @IBOutlet private weak var tripDistanceTitleLabel: UILabel!
    @IBOutlet private weak var tripDurationTitleLabel: UILabel!
    @IBOutlet private weak var tripDistanceLabel: UILabel!
    @IBOutlet private weak var tripDurationLabel: UILabel!
    @IBOutlet private weak var pilotRatingLabel: UILabel!
    @IBOutlet private weak var noRatingLabel: UILabel!
    @IBOutlet private weak var pilotRatingView: RatingView!

    var viewModel: TripDetailViewModel?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = viewModel else {
            return
        }
        bind(to: viewModel)
        setupNavigation()
        setupBackground()
        view.accessibilityIdentifier = "TripDetail"
        viewModel.viewDidLoad()
    }

    // MARK: - Private

    private func bind(to viewModel: TripDetailViewModel) {
        viewModel.visibleDetailTrip = { [weak self] detailTrip in
            guard let self = self else {
                return
            }
            let textBuilder = AttributedTextBuilder()
            let imageBuilder = ImageBuilder()

            self.setupPilotLabels(
                from: detailTrip,
                textBuilder: textBuilder,
                imageBuilder: imageBuilder
            )
            self.setupPickUpLabels(
                from: detailTrip,
                textBuilder: textBuilder,
                imageBuilder: imageBuilder
            )
            self.setupDropOffLabels(
                from: detailTrip,
                textBuilder: textBuilder,
                imageBuilder: imageBuilder
            )
            self.setupDistanceLabel(
                from: detailTrip,
                textBuilder: textBuilder
            )
            self.setupDurationLabel(
                from: detailTrip,
                textBuilder: textBuilder
            )
            self.setupPilotRating(
                from: detailTrip,
                textBuilder: textBuilder
            )
        }
        viewModel.departureTitle = { [weak self] departureTitle in
            self?.departureLabel.text = departureTitle
        }
        viewModel.arrivalTitle = { [weak self] arrivalTitle in
            self?.arrivalLabel.text = arrivalTitle
        }
        viewModel.distanceTitle = { [weak self] distanceTitle in
            self?.tripDistanceTitleLabel.text = distanceTitle
        }
        viewModel.durationTitle = { [weak self] durationTitle in
            self?.tripDurationTitleLabel.text = durationTitle
        }
        viewModel.pilotRatingTitle = { [weak self] pilotRatingTitle in
            self?.pilotRatingLabel.text = pilotRatingTitle
        }
        viewModel.noRatingTitle = { [weak self] noRatingTitle in
            let textBuilder = AttributedTextBuilder()

            self?.noRatingLabel.attributedText = textBuilder.build(
                string: noRatingTitle,
                color: #colorLiteral(red: 0.5058823529, green: 0.5058823529, blue: 0.5058823529, alpha: 1),
                size: 15,
                weight: .semibold,
                spacing: 3
            )
        }
    }

    private func setupPilotLabels(
        from detailTrip: VisibleDetailTrip,
        textBuilder: AttributedTextBuilder,
        imageBuilder: ImageBuilder
    ) {
        pilotAvatarImage.image = imageBuilder.build(from: detailTrip.avatar)
        pilotNameLabel.attributedText = textBuilder.build(
            string: detailTrip.pilotName.uppercased(),
            color: .white,
            size: 28,
            weight: .heavy,
            spacing: 5
        )
    }

    private func setupPickUpLabels(
        from detailTrip: VisibleDetailTrip,
        textBuilder: AttributedTextBuilder,
        imageBuilder: ImageBuilder
    ) {
        let formatter = TripDateFormatter()
        let date = formatter.format(date: detailTrip.pickUp.date)

        pickUpImage.image = imageBuilder.build(from: detailTrip.pickUp.picture)
        pickUpNameLabel.attributedText = textBuilder.build(
            string: detailTrip.pickUp.name.uppercased(),
            color: .white,
            size: 15,
            weight: .semibold,
            spacing: 3
        )
        departureTimeLabel.attributedText = textBuilder.build(
            string: date,
            color: #colorLiteral(red: 0.5058823529, green: 0.5058823529, blue: 0.5058823529, alpha: 1),
            size: 15,
            weight: .semibold,
            spacing: 3
        )
    }

    private func setupDropOffLabels(
        from detailTrip: VisibleDetailTrip,
        textBuilder: AttributedTextBuilder,
        imageBuilder: ImageBuilder
    ) {
        let formatter = TripDateFormatter()
        let date = formatter.format(date: detailTrip.dropOff.date)

        dropOffImage.image = imageBuilder.build(from: detailTrip.dropOff.picture)
        dropOffLabel.attributedText = textBuilder.build(
            string: detailTrip.dropOff.name.uppercased(),
            color: .white,
            size: 15,
            weight: .semibold,
            spacing: 3
        )
        arrivalTimeLabel.attributedText = textBuilder.build(
            string: date,
            color: #colorLiteral(red: 0.5058823529, green: 0.5058823529, blue: 0.5058823529, alpha: 1),
            size: 15,
            weight: .semibold,
            spacing: 3
        )
    }

    private func setupDistanceLabel(
        from detailTrip: VisibleDetailTrip,
        textBuilder: AttributedTextBuilder
    ) {
        let distanceFormatter = TripDistanceFormatter()
        let distance = distanceFormatter.format(
            distance: detailTrip.distance.value,
            unit: detailTrip.distance.unit
        )

        tripDistanceLabel.attributedText = textBuilder.build(
            string: distance,
            color: .white,
            size: 15,
            weight: .semibold,
            spacing: 3
        )
    }

    private func setupDurationLabel(
        from detailTrip: VisibleDetailTrip,
        textBuilder: AttributedTextBuilder
    ) {
        let tripDuration = TripDuration()
        let duration = tripDuration.getDuration(
            between: detailTrip.pickUp.date,
            and: detailTrip.dropOff.date
        )
        let durationFormatted = "\(duration.hour ?? 0):\(duration.minute ?? 0):\(duration.second ?? 0)"

        tripDurationLabel.attributedText = textBuilder.build(
            string: durationFormatted,
            color: .white,
            size: 15,
            weight: .semibold,
            spacing: 3
        )
    }

    private func setupPilotRating(
        from detailTrip: VisibleDetailTrip,
        textBuilder: AttributedTextBuilder
    ) {
        if detailTrip.rating > 0 {
            pilotRatingView.rating = detailTrip.rating
            noRatingLabel.isHidden = true
        } else {
            pilotRatingView.isHidden = true
        }
    }

    private func setupNavigation() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: #imageLiteral(resourceName: "back"),
            style: .plain,
            target: self,
            action: #selector(backBarButtonItemPressed(_:))
        )
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    private func setupBackground() {
        view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
    }

    @IBAction private func backBarButtonItemPressed(_ sender: Any) {
        viewModel?.didGoBack()
    }
}

extension TripDetailViewController: Storyboarded {}
