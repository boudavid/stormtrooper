//
//  VisibleDetailTrip.swift
//  Stormtrooper
//
//  Created by David Bou on 02/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

struct VisibleDetailTrip {
    let avatar: String
    let pilotName: String
    let pickUp: VisiblePointInformation
    let dropOff: VisiblePointInformation
    let distance: Distance
    let rating: Double
}

struct VisiblePointInformation {
    let name: String
    let picture: String
    let date: Date
}
