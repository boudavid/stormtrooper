//
//  TripDetailViewModel.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

final class TripDetailViewModel {
    // MARK: - Properties

    private weak var delegate: TripDetailScreenDelegate?
    private let repository: TripRepositoryType
    private let detailTrip: VisibleDetailTrip

    // MARK: - Init

    init(
        delegate: TripDetailScreenDelegate,
        repository: TripRepositoryType,
        trip: Trip
    ) {
        self.delegate = delegate
        self.repository = repository
        self.detailTrip = VisibleDetailTrip(
            avatar: trip.pilot.avatar,
            pilotName: trip.pilot.name,
            pickUp: VisiblePointInformation(
                name: trip.pickUp.name,
                picture: trip.pickUp.picture,
                date: trip.pickUp.date
            ),
            dropOff: VisiblePointInformation(
                name: trip.dropOff.name,
                picture: trip.dropOff.picture,
                date: trip.dropOff.date
            ),
            distance: trip.distance,
            rating: trip.pilot.rating
        )
    }

    // MARK: - Output

    var visibleDetailTrip: ((VisibleDetailTrip) -> Void)?
    var departureTitle: ((String) -> Void)?
    var arrivalTitle: ((String) -> Void)?
    var distanceTitle: ((String) -> Void)?
    var durationTitle: ((String) -> Void)?
    var pilotRatingTitle: ((String) -> Void)?
    var noRatingTitle: ((String) -> Void)?

    // MARK: - Input

    func viewDidLoad() {
        visibleDetailTrip?(detailTrip)
        departureTitle?("Departure")
        arrivalTitle?("Arrival")
        distanceTitle?("Trip Distance")
        durationTitle?("Trip Duration")
        pilotRatingTitle?("Pilot Rating")
        noRatingTitle?("USER DIDN'T RATE YET")
    }

    func didGoBack() {
        delegate?.didGoBack()
    }
}
