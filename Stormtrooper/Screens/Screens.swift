//
//  Screens.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

final class Screens {
    // MARK: - Properties

    let context: Context

    // MARK: - Init

    init(context: Context) {
        self.context = context
    }
}
