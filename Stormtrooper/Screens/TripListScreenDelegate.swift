//
//  TripListScreenDelegate.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

protocol TripListScreenDelegate: class {
    func didSelectTrip(_ trip: Trip)
}

extension Screens {
    func tripListViewController(delegate: TripListScreenDelegate) -> UIViewController {
        guard let controller = TripListViewController.instantiate() else {
            fatalError("Unable to instantiate \(TripListViewController.storyboardIdentifier)")
        }
        controller.viewModel = TripListViewModel(
            delegate: delegate,
            repository: context.tripRepository
        )
        return controller
    }
}
