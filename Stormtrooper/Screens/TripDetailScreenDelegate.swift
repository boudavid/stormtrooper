//
//  TripDetailScreenDelegate.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

protocol TripDetailScreenDelegate: class {
    func didGoBack()
}

extension Screens {
    func tripDetailViewController(
        delegate: TripDetailScreenDelegate,
        trip: Trip
    ) -> UIViewController {
        guard let controller = TripDetailViewController.instantiate() else {
            fatalError("Unable to instantiate \(TripDetailViewController.storyboardIdentifier)")
        }
        controller.viewModel = TripDetailViewModel(
            delegate: delegate,
            repository: context.tripRepository,
            trip: trip
        )
        return controller
    }
}
