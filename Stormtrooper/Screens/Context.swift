//
//  Context.swift
//  Stormtrooper
//
//  Created by David Bou on 02/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

final class Context {
    // MARK: - Properties

    let tripRepository: TripRepositoryType

    // MARK: - Init

    init(tripRepository: TripRepositoryType) {
        self.tripRepository = tripRepository
    }
}
