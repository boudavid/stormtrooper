//
//  TripRequest.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation

protocol TripRequestType {
    func getAll(completion: @escaping (TripResult) -> Void)
}

enum TripResult {
    case success([Trip])
    case failure
}

struct TripRequest: TripRequestType {
    let baseURL = "https://starwars.chauffeur-prive.com"
    let resourceURL: URL

    init() {
        guard let resourceURL = URL(string: baseURL) else {
            fatalError("Invalid base URL.")
        }
        self.resourceURL = resourceURL.appendingPathComponent("trips")
    }

    func getAll(completion: @escaping (TripResult) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: resourceURL) { data, _, _ in
            guard let jsonData = data else {
                completion(.failure)
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .iso8601
                let resources = try decoder.decode([Trip].self, from: jsonData)
                completion(.success(resources))
            } catch {
                completion(.failure)
            }
        }
        dataTask.resume()
    }
}
