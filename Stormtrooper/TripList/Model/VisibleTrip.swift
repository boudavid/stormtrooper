//
//  VisibleTrip.swift
//  Stormtrooper
//
//  Created by David Bou on 01/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

struct VisibleTrip {
    let avatar: String
    let pilotName: String
    let pickUpName: String
    let dropOffName: String
    let rating: Double
}
