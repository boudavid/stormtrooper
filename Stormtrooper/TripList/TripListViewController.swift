//
//  TripListViewController.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

final class TripListViewController: UIViewController {
    // MARK: - Properties

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.accessibilityIdentifier = "TripListTableView"
        }
    }

    var viewModel: TripListViewModel?
    private let source = TripListViewControllerSource()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = viewModel else {
            return
        }
        bind(to: viewModel)
        bind(to: source)
        registerNibs()
        setupBackground()
        view.accessibilityIdentifier = "TripList"
        viewModel.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }

    // MARK: - Private

    private func bind(to viewModel: TripListViewModel) {
        source.didSelectTripAt = viewModel.didSelectTrip
        viewModel.title = { [weak self] title in
            self?.navigationItem.title = title
        }
        viewModel.visibleTrips = { [weak self] trips in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.source.trips = trips
                self.tableView.reloadData()
            }
        }
        viewModel.error = { [weak self] error in
            guard let self = self else {
                return
            }
            let alert = UIAlertController(
                title: "Something went wrong",
                message: "\(error.rawValue)",
                preferredStyle: .alert
            )
            let action = UIAlertAction(
                title: "Retry",
                style: .default
            ) { _ in
                self.viewModel?.viewDidLoad()
            }

            alert.addAction(action)
            self.present(
                alert,
                animated: true,
                completion: nil
            )
        }
    }

    private func bind(to source: TripListViewControllerSource) {
        tableView.dataSource = source
        tableView.delegate = source
    }

    private func registerNibs() {
        tableView.register(TripTableViewCell.nib, forCellReuseIdentifier: TripTableViewCell.storyboardIdentifier)
    }

    private func setupNavigation() {
        navigationController?.navigationBar.shadowImage = nil
    }

    private func setupBackground() {
        view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
    }
}

extension TripListViewController: Storyboarded {}

final class TripListViewControllerSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    // MARK: - Properties

    var trips: [VisibleTrip] = []
    var didSelectTripAt: ((Int) -> Void)?

    // MARK: - UITableViewDataSource

    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return trips.count
    }

    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TripTableViewCell.storyboardIdentifier) as? TripTableViewCell else {
            fatalError("Unable to instantiate \(TripTableViewCell.storyboardIdentifier)")
        }
        cell.trip = trips[indexPath.row]
        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        didSelectTripAt?(indexPath.row)
    }
}
