//
//  TripListViewModel.swift
//  Stormtrooper
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

final class TripListViewModel {
    // MARK: - Properties

    private weak var delegate: TripListScreenDelegate?
    private let repository: TripRepositoryType
    private var trips: [Trip] = []

    // MARK: - Init

    init(
        delegate: TripListScreenDelegate,
        repository: TripRepositoryType
    ) {
        self.delegate = delegate
        self.repository = repository
    }

    // MARK: - Output

    var title: ((String) -> Void)?
    var visibleTrips: (([VisibleTrip]) -> Void)?
    var error: ((VideoRepositoryError) -> Void)?

    // MARK: - Input

    func viewDidLoad() {
        title?("LAST TRIPS")
        repository.getTrips(
            successHandler: { [weak self] trips in
                guard let self = self else {
                    return
                }
                let visibleTrips = trips.map { trip in
                    VisibleTrip(
                        avatar: trip.pilot.avatar,
                        pilotName: trip.pilot.name,
                        pickUpName: trip.pickUp.name,
                        dropOffName: trip.dropOff.name,
                        rating: trip.pilot.rating
                    )
                }

                self.trips = trips
                self.visibleTrips?(visibleTrips)
            },
            failureHandler: { [weak self] error in
                self?.error?(error)
            }
        )
    }

    func didSelectTrip(at index: Int) {
        delegate?.didSelectTrip(trips[index])
    }
}
