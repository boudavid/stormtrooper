//
//  TripTableViewCell.swift
//  Stormtrooper
//
//  Created by David Bou on 01/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import UIKit

final class TripTableViewCell: UITableViewCell {
    // MARK: - Properties

    @IBOutlet private weak var pilotAvatarImage: UIImageView! {
        didSet {
            pilotAvatarImage.toCircle(of: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25), borderWidth: 1)
        }
    }
    @IBOutlet private weak var pilotNameLabel: UILabel!
    @IBOutlet private weak var pickUpNameLabel: UILabel!
    @IBOutlet private weak var dropOffNameLabel: UILabel!
    @IBOutlet private weak var pilotRatingView: RatingView!

    var trip: VisibleTrip? {
        didSet {
            guard let trip = trip else {
                return
            }
            let textBuilder = AttributedTextBuilder()
            let imageBuilder = ImageBuilder()

            pilotNameLabel.attributedText = textBuilder.build(
                string: trip.pilotName.uppercased(),
                color: .white,
                size: 15,
                weight: .semibold,
                spacing: 3
            )
            pilotAvatarImage.image = imageBuilder.build(from: trip.avatar)
            pickUpNameLabel.text = trip.pickUpName
            dropOffNameLabel.text = trip.dropOffName
            setupPilotRating(from: trip)
        }
    }

    // MARK: - Lifecycle

    override func prepareForReuse() {
        super.prepareForReuse()
        pilotRatingView.isHidden = false
    }

    // MARK: - Private

    private func setupPilotRating(from trip: VisibleTrip) {
        if trip.rating > 0 {
            pilotRatingView.rating = trip.rating
        } else {
            pilotRatingView.isHidden = true
        }
    }
}
