//
//  TripRepositoryTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class TripRepositoryTests: XCTestCase {
    private let mockTripRequest = MockTripRequest()
    
    func testGivenSuccessfulTripRequest_WhenGettingTrips_ItCallsSuccessHandler() {
        mockTripRequest.isSuccess = true
        
        let repository = TripRepository(tripRequest: mockTripRequest)
        let expectation = self.expectation(description: "successHandler block was executed")
        
        repository.getTrips(
            successHandler: { _ in
                expectation.fulfill()
            },
            failureHandler: { _ in
                XCTFail()
            }
        )
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testGivenFailedTripRequest_WhenGettingTrips_ItCallsFailureHandler() {
        let repository = TripRepository(tripRequest: mockTripRequest)
        let expectation = self.expectation(description: "failureHandler block was executed")
        
        repository.getTrips(
            successHandler: { _ in
                XCTFail()
            },
            failureHandler: { _ in
                expectation.fulfill()
            }
        )
        
        waitForExpectations(timeout: 1, handler: nil)
    }
}
