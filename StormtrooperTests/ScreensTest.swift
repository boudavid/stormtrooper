//
//  ScreensTest.swift
//  StormtrooperTests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class ScreensTests: XCTestCase {
    private let screens = Screens(context: mockContext)

    func testTripListViewController() {
        let controller = screens.tripListViewController(delegate: MockTripListScreenDelegate())
        
        XCTAssertNotNil(controller)
    }
    
    func testTripDetailViewController() {
        let controller = screens.tripDetailViewController(
            delegate: MockTripDetailScreenDelegate(),
            trip: mockTrip
        )
        
        XCTAssertNotNil(controller)
    }
}
