//
//  TripListViewModelTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class TripListViewModelTests: XCTestCase {
    private let mockDelegate = MockTripListScreenDelegate()
    private let mockRepository = MockTripRepository()
    
    func testWhenViewDidLoad_ItOutputsTheTitle() {
        let viewModel = TripListViewModel(
            delegate: mockDelegate,
            repository: mockRepository
        )
        let expectation = self.expectation(description: "title block was executed")
        
        viewModel.title = { title in
            XCTAssertEqual(title, "LAST TRIPS")
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testGivenFailedGetTrips_WhenViewDidLoad_ItOutputsTheTrips() {
        let viewModel = TripListViewModel(
            delegate: mockDelegate,
            repository: mockRepository
        )
        let expectation = self.expectation(description: "error block was executed")
        
        viewModel.error = { error in
            XCTAssertEqual(error, .getTripsFailed)
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testGivenSuccessfulEmptyGetTrips_WhenViewDidLoad_ItOutputsTheTrips() {
        mockRepository.isSuccess = true
        
        let viewModel = TripListViewModel(
            delegate: mockDelegate,
            repository: mockRepository
        )
        let expectation = self.expectation(description: "visibleTrips block was executed")
        
        viewModel.visibleTrips = { trips in
            XCTAssertTrue(trips.isEmpty)
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testGivenSuccessfulGetTrips_WhenViewDidLoad_ItOutputsTheTrips() {
        mockRepository.isSuccess = true
        mockRepository.mockTrips = [
            mockTrip
        ]
        
        let viewModel = TripListViewModel(
            delegate: mockDelegate,
            repository: mockRepository
        )
        let expectation = self.expectation(description: "visibleTrips block was executed")
        
        viewModel.visibleTrips = { trips in
            XCTAssertEqual(trips.count, 1)
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testDidSelectTrip() {
        mockDelegate.expectation = expectation(description: "didSelectTrip was called")
        mockRepository.isSuccess = true
        mockRepository.mockTrips = [
            mockTrip
        ]
        
        let viewModel = TripListViewModel(
            delegate: mockDelegate,
            repository: mockRepository
        )
        
        viewModel.viewDidLoad()
        viewModel.didSelectTrip(at: 0)
        
        waitForExpectations(timeout: 1, handler: nil)
    }
}
