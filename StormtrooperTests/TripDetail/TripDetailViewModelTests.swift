//
//  TripDetailViewModelTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class TripDetailViewModelTests: XCTestCase {
    private let mockDelegate = MockTripDetailScreenDelegate()
    private let mockRepository = MockTripRepository()
    
    func testWhenViewDidLoad_ItOutputsVisibleDetailTrip() {
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        let expectation = self.expectation(description: "visibleDetailTrip block was executed")
        
        viewModel.visibleDetailTrip = { _ in
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testWhenViewDidLoad_ItOutputsDepartureTitle() {
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        let expectation = self.expectation(description: "departureTitle block was executed")
        
        viewModel.departureTitle = { title in
            XCTAssertEqual(title, "Departure")
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testWhenViewDidLoad_ItOutputsArrivalTitle() {
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        let expectation = self.expectation(description: "arrivalTitle block was executed")
        
        viewModel.arrivalTitle = { title in
            XCTAssertEqual(title, "Arrival")
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testWhenViewDidLoad_ItOutputsDistanceTitle() {
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        let expectation = self.expectation(description: "distanceTitle block was executed")
        
        viewModel.distanceTitle = { title in
            XCTAssertEqual(title, "Trip Distance")
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testWhenViewDidLoad_ItOutputsDurationTitle() {
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        let expectation = self.expectation(description: "durationTitle block was executed")
        
        viewModel.durationTitle = { title in
            XCTAssertEqual(title, "Trip Duration")
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testWhenViewDidLoad_ItOutputsPilotRatingTitle() {
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        let expectation = self.expectation(description: "pilotRatingTitle block was executed")
        
        viewModel.pilotRatingTitle = { title in
            XCTAssertEqual(title, "Pilot Rating")
            expectation.fulfill()
        }
        viewModel.viewDidLoad()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testDidGoBack() {
        mockDelegate.expectation = self.expectation(description: "didGoBack was called")
        
        let viewModel = TripDetailViewModel(
            delegate: mockDelegate,
            repository: mockRepository,
            trip: mockTrip
        )
        
        viewModel.didGoBack()
        
        waitForExpectations(timeout: 1, handler: nil)
    }
}
