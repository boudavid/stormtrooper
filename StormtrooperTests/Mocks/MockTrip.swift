//
//  MockTrip.swift
//  StormtrooperTests
//
//  Created by David Bou on 02/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import Foundation
@testable import Stormtrooper

let mockTrip = Trip(
    id: 1,
    pilot: Pilot(
        name: "",
        avatar: "",
        rating: 0
    ),
    distance: Distance(
        value: 0,
        unit: ""
    ),
    duration: 0,
    pickUp: PointInformation(
        name: "",
        picture: "",
        date: Date()
    ),
    dropOff: PointInformation(
        name: "",
        picture: "",
        date: Date()
    )
)

