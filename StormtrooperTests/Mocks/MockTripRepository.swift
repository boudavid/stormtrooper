//
//  MockTripRepository.swift
//  StormtrooperTests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

@testable import Stormtrooper

final class MockTripRepository: TripRepositoryType {
    var isSuccess = false
    var mockTrips: [Trip] = []
    
    func getTrips(
        successHandler: @escaping (([Trip]) -> Void),
        failureHandler: @escaping ((VideoRepositoryError) -> Void)
    ) {
        if isSuccess {
            successHandler(mockTrips)
        } else {
            failureHandler(.getTripsFailed)
        }
    }
}
