//
//  MockContext.swift
//  StormtrooperTests
//
//  Created by David Bou on 02/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

@testable import Stormtrooper

var mockContext = Context(tripRepository: MockTripRepository())
