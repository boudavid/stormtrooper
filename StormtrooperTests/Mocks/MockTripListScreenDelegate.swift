//
//  MockTripListScreenDelegate.swift
//  StormtrooperTests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class MockTripListScreenDelegate: TripListScreenDelegate {
    var expectation: XCTestExpectation?
    
    func didSelectTrip(_ trip: Trip) {
        expectation?.fulfill()
    }
}
