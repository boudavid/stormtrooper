//
//  MockTripRequest.swift
//  StormtrooperTests
//
//  Created by David Bou on 01/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

@testable import Stormtrooper

final class MockTripRequest: TripRequestType {
    var isSuccess = false
    
    func getAll(completion: @escaping (TripResult) -> Void) {
        if isSuccess {
            completion(.success([]))
        } else {
            completion(.failure)
        }
    }
}
