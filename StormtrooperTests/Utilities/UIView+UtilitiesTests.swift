//
//  UIView+UtilitiesTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 02/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class UIViewUtilitiesTests: XCTestCase {
    func testToCircle() {
        let frame = CGRect(
            x: 0,
            y: 0,
            width: 100,
            height: 100
        )
        let view = UIView(frame: frame)
        
        view.toCircle(of: .white, borderWidth: 1)
        
        XCTAssertEqual(view.layer.cornerRadius, 50)
        XCTAssertEqual(view.layer.borderWidth, 1)
        XCTAssertEqual(view.layer.borderColor, UIColor.white.cgColor)
        XCTAssertTrue(view.layer.masksToBounds)
    }
}
