//
//  TripDistanceFormatterTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class TripDistanceFormatterTests: XCTestCase {
    func testGivenBillions_WhenFormatting_ItAddsTheCorrectNumberOfCommas() {
        let formatter = TripDistanceFormatter()
        let distanceFormatted = formatter.format(
            distance: 123456789000,
            unit: "km"
        )
        
        XCTAssertEqual(distanceFormatted, "123,456,789,000 KM")
    }
    
    func testGivenMillions_WhenFormatting_ItAddsTheCorrectNumberOfCommas() {
        let formatter = TripDistanceFormatter()
        let distanceFormatted = formatter.format(
            distance: 123456789,
            unit: "km"
        )
        
        XCTAssertEqual(distanceFormatted, "123,456,789 KM")
    }
    
    func testGivenThousands_WhenFormatting_ItAddsTheCorrectNumberOfCommas() {
        let formatter = TripDistanceFormatter()
        let distanceFormatted = formatter.format(
            distance: 123456,
            unit: "km"
        )
        
        XCTAssertEqual(distanceFormatted, "123,456 KM")
    }
    
    func testGivenHundreds_WhenFormatting_ItDoesntAddCommas() {
        let formatter = TripDistanceFormatter()
        let distanceFormatted = formatter.format(
            distance: 123,
            unit: "km"
        )
        
        XCTAssertEqual(distanceFormatted, "123 KM")
    }
}
