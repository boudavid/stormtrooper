//
//  TripDateFormatterTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class TripDateFormatterTests: XCTestCase {
    func testGivenRightDate_WhenFormatting_ItOutputsTheDateFormatted() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: "2017-12-09T14:12:51Z")!
        let formatter = TripDateFormatter()
        let dateFormatted = formatter.format(date: date)
        
        XCTAssertEqual(dateFormatted, "2:12 PM")
    }
}
