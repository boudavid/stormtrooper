//
//  ImageBuilderTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 02/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class ImageBuilderTests: XCTestCase {
    func testGivenRightPath_WhenBuilding_ItReturnsAnImage() {
        let imageBuilder = ImageBuilder()
        let image = imageBuilder.build(from: "/static/admiral-ackbar.png")
        
        XCTAssertNotNil(image)
    }
    
    func testGivenWrongPath_WhenBuilding_ItDoesntReturnAnImage() {
        let imageBuilder = ImageBuilder()
        let image = imageBuilder.build(from: "wrong path")
        
        XCTAssertNil(image)
    }
}
