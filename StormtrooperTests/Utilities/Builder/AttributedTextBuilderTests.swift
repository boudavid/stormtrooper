//
//  AttributedTextBuilderTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class AttributedTextBuilderTests: XCTestCase {
    func testBuild() {
        let builder = AttributedTextBuilder()
        let text = builder.build(
            string: "Foo",
            color: .white,
            size: 15,
            weight: .regular,
            spacing: 3
        )
        
        XCTAssertNotNil(text)
    }
}
