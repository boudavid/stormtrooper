//
//  TripDurationTests.swift
//  StormtrooperTests
//
//  Created by David Bou on 03/11/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest
@testable import Stormtrooper

final class TripDurationTests: XCTestCase {
    func testGivenSuperiorEverywhereSecondDate_WhenGettingDuration_ItOutputsTheRightComponents() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let firstDate = dateFormatter.date(from: "2017-12-09T14:12:51Z")!
        let secondDate = dateFormatter.date(from: "2017-12-09T19:15:58Z")!
        let tripDuration = TripDuration()
        let dateComponents = tripDuration.getDuration(
            between: firstDate,
            and: secondDate
        )
        
        XCTAssertEqual(dateComponents.hour, 5)
        XCTAssertEqual(dateComponents.minute, 3)
        XCTAssertEqual(dateComponents.second, 7)
    }
    
    func testGivenSuperiorInHourSecondDate_WhenGettingDuration_ItOutputsTheRightComponents() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let firstDate = dateFormatter.date(from: "2017-12-09T14:12:51Z")!
        let secondDate = dateFormatter.date(from: "2017-12-09T19:08:49Z")!
        let tripDuration = TripDuration()
        let dateComponents = tripDuration.getDuration(
            between: firstDate,
            and: secondDate
        )
        
        XCTAssertEqual(dateComponents.hour, 4)
        XCTAssertEqual(dateComponents.minute, 55)
        XCTAssertEqual(dateComponents.second, 58)
    }
}
