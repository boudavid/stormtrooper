//
//  StormtrooperUITests.swift
//  StormtrooperUITests
//
//  Created by David Bou on 31/10/2018.
//  Copyright © 2018 DavidBou. All rights reserved.
//

import XCTest

final class StormtrooperUITests: XCTestCase {
    private let app = XCUIApplication()

    override func setUp() {
        continueAfterFailure = false
    }

    func testApp() {
        app.launch()
        XCTAssertTrue(app.isDisplayingList)
        app.tables["TripListTableView"].cells.allElementsBoundByIndex.first?.tap()
        XCTAssertTrue(app.isDisplayingDetail)
        app.navigationBars.buttons.element(boundBy: 0).tap()
        XCTAssertTrue(app.isDisplayingList)
    }
}

extension XCUIApplication {
    var isDisplayingList: Bool {
        return otherElements["TripList"].exists
    }
    var isDisplayingDetail: Bool {
        return otherElements["TripDetail"].exists
    }
}
